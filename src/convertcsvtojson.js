const path = require('path');
const csv = require('csvtojson');
const fs = require('fs')


const dirname = require.main.path

const matchesCsvPath = './data/matches.csv';
const deliveriesCsvPath = './data/deliveries.csv';

const saveFilePath = filePath => filePath.replace('.csv', '.json');
async function convertCsvToJson(filePath){
    return await csv().fromFile(path.resolve(dirname, filePath));

}

/**
 * Write Object as Json to file
 * @param data  object that is to be converted to json
 */
function writeJsonFile(data){
    convertCsvToJson(data).then(data=>{
        const outFilePath = saveFilePath(data);
        fs.writeFile(path.resolve(dirname, outFilePath), JSON.stringify(data),err => {
            if (err) throw err;
            console.log({data}, 'saved!');    
        })
    })
}

writeJsonFile(matchesCsvPath);
writeJsonFile(deliveriesCsvPath);