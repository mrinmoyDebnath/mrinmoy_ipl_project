/**
 * get no of matches per season
 * @param {object} matches - Json total matches object
 * @returns {object} - matches per season
 */
function getMatchesPerYear(matches) {
    return matches.reduce((matchPerYear, match) => {
        if (!matchPerYear[match.season]) matchPerYear[match.season] = 1
        else matchPerYear[match.season] += 1
        return matchPerYear
    }, {})
}

/**
 * get no of matches won per team per year
 * @param {object} matches -Json total matches object
 * @returns {object} - matches won per team per year
 */
function getMatchesWonPerTeam(matches) {
    return matches.reduce((allSeasons, match) => {
        if (allSeasons.indexOf(match.season) === -1) return [...allSeasons, match.season]
        else return allSeasons
    }, []).map(year => {
        let y = matches.reduce((allWinners, match) => {
            if (match.season === year) {
                if (!(allWinners[match.winner])) allWinners[match.winner] = 1
                else allWinners[match.winner] += 1
            }
            return allWinners
        }, {})
        return { [year]: y }
    })
}

/**
 * get runs conceded by every team for a particular year
 * @param {object} matches - Json total matches object
 * @param {object} deliveries - Json total deliveries object 
 * @param {string} year - year
 * @returns {object} - runs conceded per team
 */
function getRunsConcededPerTeam(matches, deliveries, year) {
    const matchesOfTheYear = matches.reduce((allMatchIDs, match) => {
        if (match.season === year) {
            return [...allMatchIDs, match.id]
        } else return allMatchIDs
    }, [])

    return deliveries.reduce((allMatchDeliveries, delivery) => {
        if (matchesOfTheYear.indexOf(delivery.match_id) !== -1) return [...allMatchDeliveries, delivery]
        else return [...allMatchDeliveries]
    }, []).reduce((allRunsConceded, over) => {
        if (!(allRunsConceded[over.bowling_team])) {
            allRunsConceded[over.bowling_team] = Number.parseInt(over.extra_runs)
            return allRunsConceded
        } else {
            allRunsConceded[over.bowling_team] += Number.parseInt(over.extra_runs)
            return allRunsConceded
        }
    }, {})
}

/**
 * get top 10 economy bowler of a particular year
 * @param {object} matches - Json total matches object
 * @param {object} deliveries - Json total deliveries object
 * @param {string} year - year
 * @returns {object} - top 10 bowlers with their their runs conceded, balls delivered and economies
 */
function getEconomyBowlers(matches, deliveries, year) {
    const matchesOfTheYear = matches.reduce((allMatchIDs, match) => {
        if (match.season === year) {
            return [...allMatchIDs, match.id]
        } else return allMatchIDs
    }, [])

    bowlersOfTheYear = deliveries.reduce((allDeliveries, delivery) => {
        if (matchesOfTheYear.indexOf(delivery.match_id) !== -1) return [...allDeliveries, delivery]
        else return [...allDeliveries]
    }, []).reduce((allBowlers, delivery) => {
        if (!(allBowlers[delivery.bowler])) {
            allBowlers[delivery.bowler] = {
                balls: 1,
                runs: Number.parseInt(delivery.total_runs)
            }
            return allBowlers
        } else {
            allBowlers[delivery.bowler].balls += 1;
            allBowlers[delivery.bowler].runs += Number.parseInt(delivery.total_runs);
            return allBowlers
        }
    }, {})

    const allBowlers = Object.entries(bowlersOfTheYear).map(bowler => {
        const bowlerName = bowler[0];
        const bowlerEconomy = bowler[1].runs / bowler[1].balls;
        return Object.assign({ 'name': bowlerName }, bowler[1], { 'economy':  bowlerEconomy})
    })

    return allBowlers.sort((bowler1, bowler2) => {
        if (bowler1.economy < bowler2.economy) return -1
        else return 1
    }).slice(0, 10)
}

/**
 * get total wickets taken by a bowler in a particular season
 * @param {object} matches - Json total matches object
 * @param {object} deliveries - Json total deliveries object
 * @param {string} bowlerName - Name of bowler
 * @param {string} year - year
 * @returns {number} - total wickets
 */
function getWicketsOfBowler(matches, deliveries, bowlerName, year) {
    const matchesOfTheYear = matches.reduce((allMatchIds, match) => {
        if (match.season === year) {
            return [...allMatchIds, match.id]
        } else return allMatchIds
    }, [])
    return deliveries.reduce((wicketCount, delivery) => {
        if (matchesOfTheYear.includes(delivery.match_id) && delivery.dismissal_kind !== '' && delivery.dismissal_kind !== 'run out' && delivery.bowler === bowlerName) {
            return wicketCount + 1
        }
        return wicketCount
    }, 0)
}

/**
 * get total boundaries(4s and 6s) of a batsman for a particular season
 * @param {object} matches - Json total matches object
 * @param {object} deliveries - Json total deliveries object
 * @param {string} batsman - name of the batsman
 * @param {string} year  - year
 * @returns {number} - total boundaries
 */
function getBatsmanTotalBoundaries(matches, deliveries, batsman, year) {
    const matchesOfTheYear = matches.reduce((allMatchIDs, match) => {
        if (match.season === year) {
            return [...allMatchIDs, match.id]
        } else return allMatchIDs
    }, [])
    return deliveries.reduce((batsmanBoundaries, delivery) => {
        if (matchesOfTheYear.includes(delivery.match_id) && (delivery.batsman_runs == '6' || delivery.batsman_runs == '4') && delivery.batsman === batsman) {
            return batsmanBoundaries + 1
        }
        return batsmanBoundaries
    }, 0)
}

/**
 * get total bowled wickets by a bowler for a particular season
 * @param {object} matches - Json total matches object
 * @param {object} deliveries - Json total deliveries object
 * @param {string} bowlerName - name of bowler
 * @param {string} year - year
 * @returns {number} - total wickets taken by bowled
 */
function getPlayersBowledByBowler(matches, deliveries, bowlerName, year) {
    const matchesOfTheYear = matches.reduce((allMatchIDs, match) => {
        if (match.season === year) {
            return [...allMatchIDs, match.id]
        } else return allMatchIDs
    }, [])
    return deliveries.reduce((bowledPlayersCount, delivery) => {
        if (matchesOfTheYear.includes(delivery.match_id) && delivery.dismissal_kind === 'bowled' && delivery.bowler === bowlerName) {
            return bowledPlayersCount + 1
        }
        return bowledPlayersCount
    }, 0)
}

/**
 * get total no balls delivered by a team in a particular year
 * @param {object} matches - Json total matches object
 * @param {object} deliveries - Json total deliveries object
 * @param {string} team - team name
 * @param {string} year - year
 */
function getTotalNoBallsByTeam(matches, deliveries, team, year) {
    const matchesOfTheYear = matches.reduce((allMatchIDs, match) => {
        if (match.season === year) {
            return [...allMatchIDs, match.id]
        } else return allMatchIDs
    }, [])
    return deliveries.reduce((noBallCount, delivery) => {
        if (matchesOfTheYear.includes(delivery.match_id) && delivery.noball_runs !== '0' && delivery.bowling_team === team) {
            return noBallCount + 1
        }
        return noBallCount
    }, 0)
}

module.exports = {
    getMatchesPerYear,
    getMatchesWonPerTeam,
    getRunsConcededPerTeam,
    getEconomyBowlers,
    getWicketsOfBowler,
    getBatsmanTotalBoundaries,
    getPlayersBowledByBowler,
    getTotalNoBallsByTeam
}