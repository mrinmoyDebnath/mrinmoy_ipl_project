const path = require('path');
const fs = require('fs');
const ipl = require('./ipl');

const dirname = require.main.path;
const matchesJsonPath = '../data/matches.json';
const deliveriesJsonPath = '../data/deliveries.json';

const matches = JSON.parse(fs.readFileSync(path.resolve(dirname, matchesJsonPath)))
const deliveries = JSON.parse(fs.readFileSync(path.resolve(dirname, deliveriesJsonPath)))

/**
 * write data to a file as json
 * @param {object} data - data to be saved as json
 * @param {filePath} outputPath - path where data has to be saved
 */
function writeJsonToOutput(data, outputPath){
    fs.writeFile(path.resolve(dirname, `../public/output/${outputPath}.json`), JSON.stringify(data), (err) => {
        if (err) throw err;
        else console.log(`successfully wrote ${outputPath}`)
    });
}

// problem 1
const matchesPerYear = ipl.getMatchesPerYear(matches)
writeJsonToOutput(matchesPerYear,`matchesPerYear`);

//problem 2
const matchesWonPerTeam = ipl.getMatchesWonPerTeam(matches)
writeJsonToOutput(matchesWonPerTeam,`matchesWonPerTeam`);

//problem 3
const runsConcededPerTeam = ipl.getRunsConcededPerTeam(matches, deliveries, '2016')
writeJsonToOutput(runsConcededPerTeam,`runsConcededPerTeam`);

//problem 4
const EconomyBowlers = ipl.getEconomyBowlers(matches, deliveries, '2015')
writeJsonToOutput(EconomyBowlers,`EconomyBowlers`);

//problem 5
const wicketsOfBowlers = ipl.getWicketsOfBowler(matches, deliveries, 'JJ Bumrah', '2016')
console.log({wicketsOfBowlers});

//problem 6
const batsmanTotalBoundaries = ipl.getBatsmanTotalBoundaries(matches, deliveries, 'MS Dhoni', '2015')
console.log({batsmanTotalBoundaries});

//problem 7
const playersBowledByBowler = ipl.getPlayersBowledByBowler(matches, deliveries, 'JJ Bumrah', '2016')
console.log({playersBowledByBowler});

//problem 8
const totalNoBallsByTeam = ipl.getTotalNoBallsByTeam(matches, deliveries, 'Royal Challengers Bangalore', '2015')
console.log({totalNoBallsByTeam});
